json.extract! student, :id, :rollno, :name, :created_at, :updated_at
json.url student_url(student, format: :json)
